package br.com.sicredi.events.domain.mapper

import br.com.sicredi.events.data.model.Event
import br.com.sicredi.events.domain.model.DetailedTimelineEvent
import br.com.sicredi.events.util.SDF
import java.util.*

object DetailedTimelineEventMapper : Mapper<Event, DetailedTimelineEvent> {

    override fun map(input: Event) = DetailedTimelineEvent(
        date = SDF.format(Date(input.date)),
        description = input.description,
        price = input.price,
        image = input.image,
        title = input.title,
        id = input.id,
        latitude = input.latitude,
        longitude = input.longitude,
    )

}