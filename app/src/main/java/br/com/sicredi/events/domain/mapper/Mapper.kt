package br.com.sicredi.events.domain.mapper

interface Mapper<I, O> {

    fun map(input: I): O

}