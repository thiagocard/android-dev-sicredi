package br.com.sicredi.events.domain.mapper

import br.com.sicredi.events.data.model.Event
import br.com.sicredi.events.domain.model.TimelineEvent
import br.com.sicredi.events.util.SDF
import java.util.*

object TimelineEventMapper : Mapper<Event, TimelineEvent> {

    override fun map(input: Event) = TimelineEvent(
        date = SDF.format(Date(input.date)),
        description = input.description,
        price = input.price,
        image = input.image,
        title = input.title,
        id = input.id
    )

}