package br.com.sicredi.events.ui.events

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import br.com.sicredi.events.domain.repository.EventsRepository
import br.com.sicredi.events.ui.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class EventsViewModel(
    private val eventsRepository: EventsRepository
) : ViewModel() {

    private val _eventsFlow = MutableStateFlow<ViewState<EventsState>>(Loading)
    val eventsFlow: StateFlow<ViewState<EventsState>> = _eventsFlow

    fun fetchEvents() {
        viewModelScope.launch {
            val result = eventsRepository.events()
            result.getOrNull()?.let { events ->
                _eventsFlow.value = Loaded(EventsState(events))
            } ?: result.exceptionOrNull()?.let { _eventsFlow.value = Error(it) }
        }
    }

}