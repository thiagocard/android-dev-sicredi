package br.com.sicredi

import android.app.Application
import br.com.sicredi.events.data.EventsApi
import br.com.sicredi.events.data.model.Event
import br.com.sicredi.events.domain.mapper.DetailedTimelineEventMapper
import br.com.sicredi.events.domain.mapper.Mapper
import br.com.sicredi.events.domain.mapper.TimelineEventMapper
import br.com.sicredi.events.domain.model.DetailedTimelineEvent
import br.com.sicredi.events.domain.model.TimelineEvent
import br.com.sicredi.events.domain.repository.EventsRepository
import br.com.sicredi.events.domain.repository.EventsRepositoryImpl
import br.com.sicredi.events.ui.event_details.EventDetailViewModel
import br.com.sicredi.events.ui.events.EventsViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import org.koin.core.qualifier.named
import org.koin.dsl.module

class EventsApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@EventsApplication)
            androidLogger(if (BuildConfig.DEBUG) Level.ERROR else Level.NONE)
            modules(
                module {
                    single { EventsApi() }
                    single { (get() as EventsApi).eventsService }
                    single { (get() as EventsApi).checkinService }
                    single<Mapper<Event, TimelineEvent>>(named("event-mapper")) { TimelineEventMapper }
                    single<Mapper<Event, DetailedTimelineEvent>>(named("detail-mapper")) { DetailedTimelineEventMapper }
                    factory<EventsRepository> {
                        EventsRepositoryImpl(
                            get(),
                            get(),
                            get(named("event-mapper")),
                            get(named("detail-mapper")),
                        )
                    }
                    viewModel { EventsViewModel(get()) }
                    viewModel { EventDetailViewModel(get()) }
                }
            )
        }
    }

}