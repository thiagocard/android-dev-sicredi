package br.com.sicredi.events.domain.repository

import br.com.sicredi.events.data.model.CheckinBody
import br.com.sicredi.events.domain.model.DetailedTimelineEvent
import br.com.sicredi.events.domain.model.TimelineEvent

interface EventsRepository {

    suspend fun events(): Result<List<TimelineEvent>>

    suspend fun event(id: Int): Result<DetailedTimelineEvent>

    suspend fun checkin(body: CheckinBody): Result<Unit>

}