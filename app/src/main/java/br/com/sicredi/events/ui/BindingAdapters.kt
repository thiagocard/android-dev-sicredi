package br.com.sicredi.events.ui

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import coil.load

object BindingAdapters {

    @BindingAdapter("src_url")
    @JvmStatic
    fun setImageUrl(view: ImageView, url: String?) {
        url?.let(view::load)
    }

}