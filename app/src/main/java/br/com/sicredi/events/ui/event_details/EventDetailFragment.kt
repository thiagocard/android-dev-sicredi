package br.com.sicredi.events.ui.event_details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import br.com.sicredi.BR
import br.com.sicredi.databinding.FragmentEventDetailBinding
import br.com.sicredi.events.ui.Error
import br.com.sicredi.events.ui.Idle
import br.com.sicredi.events.ui.Loaded
import br.com.sicredi.events.ui.Loading
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class EventDetailFragment : Fragment() {

    private var _binding: FragmentEventDetailBinding? = null
    private val binding get() = _binding!!

    private val viewModel: EventDetailViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentEventDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.fetchEvent(arguments?.getInt("event-id"))

        lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.RESUMED) {
                launch {
                    viewModel.eventFlow.collectLatest { state ->
                        when (state) {
                            is Error -> {
                                binding.progressBar.isVisible = false
                                showError(state)
                            }
                            Idle -> {}
                            is Loaded -> {
                                binding.progressBar.isVisible = false
                                binding.constraintLayout.isVisible = true
                                binding.btnCheckin.setOnClickListener {
                                    state.result.data.let { event ->  viewModel.checkin(event.title, event.id) }
                                }
                                binding.setVariable(BR.event, state.result.data)
                            }
                            Loading -> {
                                binding.progressBar.isVisible = true
                            }
                        }
                    }
                }

                launch {
                    viewModel.checkinFlow
                        .collectLatest {
                            when (it) {
                                is Idle -> {}
                                is Error -> showError(it)
                                is Loaded -> {
                                    Snackbar.make(
                                        binding.root,
                                        "Checked-in to ${it.result.eventName}",
                                        Snackbar.LENGTH_LONG
                                    ).show()
                                }
                                Loading -> {}
                            }
                        }
                }
            }
        }
    }

    private fun showError(it: Error) {
        Snackbar.make(
            binding.root,
            it.throwable.message ?: "Error",
            Snackbar.LENGTH_LONG
        ).show()
    }

}