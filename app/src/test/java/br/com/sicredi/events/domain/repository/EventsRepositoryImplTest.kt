package br.com.sicredi.events.domain.repository

import br.com.sicredi.events.data.model.CheckinBody
import br.com.sicredi.events.data.model.Event
import br.com.sicredi.events.data.service.CheckinService
import br.com.sicredi.events.data.service.EventsService
import br.com.sicredi.events.domain.mapper.DetailedTimelineEventMapper
import br.com.sicredi.events.domain.mapper.TimelineEventMapper
import io.mockk.coEvery
import io.mockk.mockk
import io.mockk.spyk
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Test
import retrofit2.Response
import kotlin.random.Random

@ExperimentalCoroutinesApi
class EventsRepositoryImplTest {

    private val eventsService = mockk<EventsService>()
    private val checkinService = mockk<CheckinService>()
    private val timelineEventMapper = spyk<TimelineEventMapper>()
    private val detailedTimelineEventMapper = spyk<DetailedTimelineEventMapper>()
    private val repository = EventsRepositoryImpl(
        eventsService,
        checkinService,
        timelineEventMapper,
        detailedTimelineEventMapper,
    )

    private val randomEvents: List<Event> by lazy {
        (1..10).map { number ->
            Event(
                people = listOf(),
                date = Random.nextLong(),
                description = "Lorem Ipsum",
                title = "Event #${number}",
                latitude = Random.nextDouble(),
                longitude = Random.nextDouble(),
                image = "https://url.to/events/${number}",
                price = Random.nextDouble(),
                id = number,
            )
        }
    }

    @Test
    fun `should return a list of events with success`() = runTest {
        coEvery { eventsService.events() } returns randomEvents
        val events = repository.events().getOrThrow()
        assertEquals(10, events.size)
        assertEquals(2, events[1].id)
        assertEquals("Lorem Ipsum", events.random().description)
    }

    @Test
    fun `given the id of an event, should return its details with success`() = runTest {
        coEvery { eventsService.event(1) } returns randomEvents[0]
        val event = repository.event(1).getOrThrow()
        assertEquals(1, event.id)
        assertEquals("https://url.to/events/1", event.image)
        assertEquals("Event #1", event.title)
    }

    @Test
    fun `given the id of an event, should perform a check-in to the event with success`() =
        runTest {
            coEvery { checkinService.checkin(any()) } returns Unit
            val response = repository.checkin(
                CheckinBody(
                    eventId = 1,
                    name = "Thiago",
                    email = "thiago@email.net"
                )
            ).getOrThrow()
            assertEquals(Unit, response)
        }

}