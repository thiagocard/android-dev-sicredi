package br.com.sicredi.events.ui.events

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import br.com.sicredi.R
import br.com.sicredi.databinding.FragmentEventsBinding
import br.com.sicredi.events.ui.*
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel

class EventsFragment : Fragment() {

    private var _binding: FragmentEventsBinding? = null
    private val binding get() = _binding!!

    private val viewModel: EventsViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentEventsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.fetchEvents()

        lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.RESUMED) {

                viewModel.eventsFlow
                    .collectLatest {
                        when (it) {
                            is Idle -> {}
                            is Error -> {
                                binding.progressBar.isVisible = false
                                showError(it)
                            }
                            is Loaded -> {
                                binding.progressBar.isVisible = false
                                binding.recyclerView.adapter = EventsAdapter(
                                    it.result.data
                                ) { event ->
                                    (activity as MainActivity).navController.navigate(
                                        R.id.event_details,
                                        bundleOf("event-id" to event.id)
                                    )
                                }
                            }
                            Loading -> binding.progressBar.isVisible = true
                        }
                    }
            }
        }
    }

    private fun showError(it: Error) {
        Snackbar.make(
            binding.root,
            it.throwable.message ?: "Error",
            Snackbar.LENGTH_LONG
        ).show()
    }

}