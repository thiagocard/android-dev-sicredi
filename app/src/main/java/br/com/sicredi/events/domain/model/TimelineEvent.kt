package br.com.sicredi.events.domain.model

data class TimelineEvent(
    val date: String,
    val description: String,
    val image: String,
    val price: Double,
    val title: String,
    val id: Int,
)
