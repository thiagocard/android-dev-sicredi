package br.com.sicredi.events.data.model

import kotlinx.serialization.Serializable

@Serializable
data class CheckinBody(
    val eventId: Int,
    val name: String,
    val email: String
)
