package br.com.sicredi.events.ui

sealed interface ViewState<out T>

object Idle : ViewState<Nothing>
object Loading : ViewState<Nothing>
data class Loaded<T>(val result: T): ViewState<T>
data class Error(val throwable: Throwable): ViewState<Nothing>

