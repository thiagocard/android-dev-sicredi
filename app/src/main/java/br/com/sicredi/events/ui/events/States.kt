package br.com.sicredi.events.ui.events

import br.com.sicredi.events.domain.model.DetailedTimelineEvent
import br.com.sicredi.events.domain.model.TimelineEvent

data class CheckinState(
    val eventName: String,
    val success: Boolean
)

data class EventsState(
    val data: List<TimelineEvent>
)

data class EventDetailState(
    val data: DetailedTimelineEvent
)