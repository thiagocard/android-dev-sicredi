package br.com.sicredi.events.data.service

import br.com.sicredi.events.data.EventsApi
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import java.io.File
import java.net.HttpURLConnection

open class ServiceTest {

    protected val server = MockWebServer()

    protected val api = EventsApi(apiUrl = "http://localhost:8082/")

    @Before
    fun setUp() = server.start(port = 8082)

    @After
    fun tearDown() = server.shutdown()

    protected fun mockResponse(path: String? = null, sc: Int = HttpURLConnection.HTTP_OK) =
        MockResponse().apply {
            if (path != null) {
                setBody(File(this@ServiceTest.javaClass.classLoader?.getResource(path)!!.file).readText())
            }
            setResponseCode(sc)
        }

}