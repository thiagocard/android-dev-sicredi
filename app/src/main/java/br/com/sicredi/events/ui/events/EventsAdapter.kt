package br.com.sicredi.events.ui.events

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.sicredi.R
import br.com.sicredi.databinding.ItemEventBinding
import br.com.sicredi.events.domain.model.TimelineEvent
import coil.load

class EventsAdapter(
    private val list: List<TimelineEvent>,
    private val onItemClickListener: (TimelineEvent) -> Unit
) : RecyclerView.Adapter<EventViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventViewHolder {
        return EventViewHolder(
            ItemEventBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: EventViewHolder, position: Int) {
        holder.bind(list[position], onItemClickListener)
    }

    override fun getItemCount() = list.size

}

class EventViewHolder(
    private val binding: ItemEventBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(timelineEvent: TimelineEvent, onItemClickListener: (TimelineEvent) -> Unit) {
        binding.tvTitle.text = timelineEvent.title
        binding.tvDesc.text = timelineEvent.description
        binding.tvDate.text = timelineEvent.date
        binding.root.setOnClickListener { onItemClickListener(timelineEvent) }
        binding.ivImage.load(timelineEvent.image.replace("http", "https")) {
            fallback(R.drawable.ic_baseline_broken_image_24)
        }
    }

}