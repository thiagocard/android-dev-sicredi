package br.com.sicredi.events.data

import br.com.sicredi.events.data.service.CheckinService
import br.com.sicredi.events.data.service.EventsService
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit

class EventsApi(
    apiUrl: String = "https://5f5a8f24d44d640016169133.mockapi.io/api/"
) {
    private val retrofit = Retrofit.Builder()
        .baseUrl(apiUrl)
        .addConverterFactory(Json.asConverterFactory("application/json".toMediaType())).client(
            OkHttpClient.Builder()
                .addNetworkInterceptor(HttpLoggingInterceptor().apply {
                    level = HttpLoggingInterceptor.Level.BODY
                })
                .build()
        ).build()

    val eventsService: EventsService by lazy { retrofit.create(EventsService::class.java) }

    val checkinService: CheckinService by lazy { retrofit.create(CheckinService::class.java) }

}
