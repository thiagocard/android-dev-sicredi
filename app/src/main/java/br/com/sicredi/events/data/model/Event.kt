package br.com.sicredi.events.data.model

import kotlinx.serialization.Serializable

@Serializable
data class Event(
    val people: List<String>,
    val date: Long,
    val description: String,
    val image: String,
    val longitude: Double,
    val latitude: Double,
    val price: Double,
    val title: String,
    val id: Int,
)