package br.com.sicredi.events.data.service

import br.com.sicredi.events.data.model.CheckinBody
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Test

@ExperimentalCoroutinesApi
class CheckinServiceTest : ServiceTest() {

    @Test
    fun `should check-in to an event with success`() = runTest {
        server.enqueue(mockResponse("checkin.json", sc = 201))
        val response = api.checkinService.checkin(CheckinBody(1, "name", "email"))
        assertEquals(Unit, response)
    }

}