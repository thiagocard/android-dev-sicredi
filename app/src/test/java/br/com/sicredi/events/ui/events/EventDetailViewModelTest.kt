package br.com.sicredi.events.ui.events

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import br.com.sicredi.events.domain.model.DetailedTimelineEvent
import br.com.sicredi.events.domain.repository.EventsRepository
import br.com.sicredi.events.ui.Loaded
import br.com.sicredi.events.ui.Loading
import br.com.sicredi.events.ui.event_details.EventDetailViewModel
import io.mockk.coEvery
import io.mockk.mockk
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.take
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import kotlin.random.Random

@ExperimentalCoroutinesApi
class EventDetailViewModelTest {

    private val repo = mockk<EventsRepository>()
    private val viewModel by lazy { EventDetailViewModel(repo) }

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        Dispatchers.setMain(StandardTestDispatcher())
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun `should emit loading and loaded states with success`() = runTest {
        val data = DetailedTimelineEvent(
            date = "",
            description = "",
            image = "",
            longitude = Random.nextDouble(),
            latitude = Random.nextDouble(),
            price = Random.nextDouble(),
            title = "",
            id = 1
        )
        coEvery { repo.event(any()) } returns Result.success(data)

        viewModel.fetchEvent(1)

        val states = viewModel.eventFlow.take(2).toList()

        assertEquals(Loading, states[0])
        assertEquals(Loaded(EventDetailState(data)), states[1])
    }

    @Test
    fun `should emit check-in to an event with success`() = runTest {
        coEvery { repo.checkin(any()) } returns Result.success(Unit)

        viewModel.checkin("Event #1", 1)

        val states = viewModel.checkinFlow.take(2).toList()

        assertEquals(Loading, states[0])
        assertEquals(Loaded(CheckinState("Event #1", success = true)), states[1])
    }

}