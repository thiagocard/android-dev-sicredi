package br.com.sicredi.events.util

import java.text.SimpleDateFormat
import java.util.*

val SDF = SimpleDateFormat("dd/MM/yyyy", Locale("pt", "BR"))
