package br.com.sicredi.events.ui.events

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import br.com.sicredi.events.domain.model.TimelineEvent
import br.com.sicredi.events.domain.repository.EventsRepository
import br.com.sicredi.events.ui.Error
import br.com.sicredi.events.ui.Loaded
import br.com.sicredi.events.ui.Loading
import io.mockk.coEvery
import io.mockk.mockk
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.take
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.io.IOException
import java.time.LocalDate
import kotlin.random.Random

@ExperimentalCoroutinesApi
class EventsViewModelTest {

    private val repo = mockk<EventsRepository>()
    private val viewModel by lazy { EventsViewModel(repo) }

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        Dispatchers.setMain(StandardTestDispatcher())
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun `should emit loading and loaded states with success`() = runTest {
        val data = listOf<TimelineEvent>()
        coEvery { repo.events() } returns Result.success(data)

        viewModel.fetchEvents()

        val states = viewModel.eventsFlow.take(2).toList()

        assertEquals(Loading, states[0])
        assertEquals(Loaded(EventsState(data)), states[1])
    }

    @Test
    fun `should emit loading and error states when api gives errors`() = runTest {
        val exception = IOException()
        coEvery { repo.events() } returns Result.failure(exception)

        viewModel.fetchEvents()

        val states = viewModel.eventsFlow.take(2).toList()

        assertEquals(Loading, states[0])
        assertEquals(Error(exception), states[1])
    }

}