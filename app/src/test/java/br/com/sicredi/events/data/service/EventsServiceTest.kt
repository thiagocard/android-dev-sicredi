package br.com.sicredi.events.data.service

import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Test

@ExperimentalCoroutinesApi
class EventsServiceTest : ServiceTest() {

    @Test
    fun `should return a list of events with success`() = runTest {
        server.enqueue(mockResponse("events.json"))
        val events = api.eventsService.events()
        assertEquals(5, events.size)
        val firstEvent = events.first()
        assertEquals(1534784400000, firstEvent.date)
        assertEquals(-51.2146267, firstEvent.longitude)
        assertEquals(-30.0392981, firstEvent.latitude)
        assertEquals(29.99, firstEvent.price)
        assertEquals(
            "O Patas Dadas estará na Redenção, nesse domingo, com cães para adoção e produtos à venda!\n\nNa ocasião, teremos bottons, bloquinhos e camisetas!\n\nTraga seu Pet, os amigos e o chima, e venha aproveitar esse dia de sol com a gente e com alguns de nossos peludinhos - que estarão prontinhos para ganhar o ♥ de um humano bem legal pra chamar de seu. \n\nAceitaremos todos os tipos de doação:\n- guias e coleiras em bom estado\n- ração (as que mais precisamos no momento são sênior e filhote)\n- roupinhas \n- cobertas \n- remédios dentro do prazo de validade",
            firstEvent.description
        )
        assertEquals(
            "http://lproweb.procempa.com.br/pmpa/prefpoa/seda_news/usu_img/Papel%20de%20Parede.png",
            firstEvent.image
        )
        assertEquals("Feira de adoção de animais na Redenção", firstEvent.title)
        assertEquals(1, firstEvent.id)
    }

    @Test
    fun `given the event id, should return its deatils with success`() = runTest {
        server.enqueue(mockResponse("event-1.json"))
        val event = api.eventsService.event(1)
        assertEquals(1534784400000, event.date)
        assertEquals(-51.2146267, event.longitude)
        assertEquals(-30.0392981, event.latitude)
        assertEquals(29.99, event.price)
        assertEquals(
            "O Patas Dadas estará na Redenção, nesse domingo, com cães para adoção e produtos à venda!\n\nNa ocasião, teremos bottons, bloquinhos e camisetas!\n\nTraga seu Pet, os amigos e o chima, e venha aproveitar esse dia de sol com a gente e com alguns de nossos peludinhos - que estarão prontinhos para ganhar o ♥ de um humano bem legal pra chamar de seu. \n\nAceitaremos todos os tipos de doação:\n- guias e coleiras em bom estado\n- ração (as que mais precisamos no momento são sênior e filhote)\n- roupinhas \n- cobertas \n- remédios dentro do prazo de validade",
            event.description
        )
        assertEquals(
            "http://lproweb.procempa.com.br/pmpa/prefpoa/seda_news/usu_img/Papel%20de%20Parede.png",
            event.image
        )
        assertEquals("Feira de adoção de animais na Redenção", event.title)
        assertEquals(1, event.id)
    }

}