package br.com.sicredi.events.data.service

import br.com.sicredi.events.data.model.Event
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface EventsService {

    @GET("events")
    suspend fun events(): List<Event>

    @GET("events/{id}")
    suspend fun event(@Path("id") id: Int): Event

}