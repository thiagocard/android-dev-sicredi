package br.com.sicredi.events.domain.repository

import br.com.sicredi.events.data.model.CheckinBody
import br.com.sicredi.events.data.model.Event
import br.com.sicredi.events.data.service.CheckinService
import br.com.sicredi.events.data.service.EventsService
import br.com.sicredi.events.domain.mapper.Mapper
import br.com.sicredi.events.domain.model.DetailedTimelineEvent
import br.com.sicredi.events.domain.model.TimelineEvent

class EventsRepositoryImpl(
    private val eventsService: EventsService,
    private val checkinService: CheckinService,
    private val timelineEventMapper: Mapper<Event, TimelineEvent>,
    private val detailedTimelineEventMapper: Mapper<Event, DetailedTimelineEvent>
) : EventsRepository {

    override suspend fun events(): Result<List<TimelineEvent>> = kotlin.runCatching {
        eventsService.events().map { timelineEventMapper.map(it) }
    }

    override suspend fun event(id: Int): Result<DetailedTimelineEvent> = kotlin.runCatching {
        eventsService.event(id).let { detailedTimelineEventMapper.map(it) }
    }

    override suspend fun checkin(body: CheckinBody): Result<Unit> = kotlin.runCatching {
        checkinService.checkin(body)
    }

}