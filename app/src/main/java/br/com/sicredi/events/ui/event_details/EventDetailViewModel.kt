package br.com.sicredi.events.ui.event_details

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import br.com.sicredi.events.data.model.CheckinBody
import br.com.sicredi.events.domain.model.DetailedTimelineEvent
import br.com.sicredi.events.domain.model.TimelineEvent
import br.com.sicredi.events.domain.repository.EventsRepository
import br.com.sicredi.events.ui.*
import br.com.sicredi.events.ui.events.CheckinState
import br.com.sicredi.events.ui.events.EventDetailState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class EventDetailViewModel(
    private val eventsRepository: EventsRepository
) : ViewModel() {

    private val _eventFlow = MutableStateFlow<ViewState<EventDetailState>>(Loading)
    val eventFlow: StateFlow<ViewState<EventDetailState>> = _eventFlow

    private val _checkinFlow = MutableStateFlow<ViewState<CheckinState>>(Idle)
    val checkinFlow: StateFlow<ViewState<CheckinState>> = _checkinFlow

    fun fetchEvent(id: Int?) {
        id?.let {
            viewModelScope.launch {
                val result = eventsRepository.event(id)
                result.getOrNull()?.let { event ->
                    _eventFlow.value = Loaded(EventDetailState(event))
                } ?: result.exceptionOrNull()?.let { _eventFlow.value = Error(it) }
            }
        }
    }

    fun checkin(name: String, eventId: Int) {
        _checkinFlow.value = Loading
        viewModelScope.launch {
            val result = eventsRepository.checkin(
                CheckinBody(
                    eventId = eventId,
                    name = "username",
                    email = "user@email.com"
                )
            )
            result.getOrNull()?.let {
                _checkinFlow.value = Loaded(CheckinState(name, success = true))
            } ?: result.exceptionOrNull()?.let { _checkinFlow.value = Error(it) }
        }
    }

}
