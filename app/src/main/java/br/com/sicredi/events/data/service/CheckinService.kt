package br.com.sicredi.events.data.service

import br.com.sicredi.events.data.model.CheckinBody
import retrofit2.http.Body
import retrofit2.http.POST

interface CheckinService {

    @POST("checkin")
    suspend fun checkin(@Body body: CheckinBody)
}