package br.com.sicredi.events.domain.model

data class DetailedTimelineEvent(
    val date: String,
    val description: String,
    val image: String,
    val longitude: Double,
    val latitude: Double,
    val price: Double,
    val title: String,
    val id: Int,
)
